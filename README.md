# docker-kube-deploy

[![License][license-badge]][license]
[![standard-readme compliant][readme-badge]][readme-link]

> Apply a deployment to a Kubernetes cluster

Apply a download to a Kubernetes cluster.
Currently, only Google Container Engine is supported.


## Usage

```
docker run -it \
   -e "GCLOUD_PROJECTID=project-123" \
   -e "GCLOUD_ZONE=us-central1-a" \
   -e "GCLOUD_CLUSTER=cluster-1" \
   -e "GCLOUD_SERVICEACCOUNT_BASE64=abc123..." \
   -v "$(pwd)/deployment.yml:/deployment.yml" \
   mreil/kube-deploy
```

[Docker Hub][hub]

### Environment

* GCLOUD_PROJECTID
* GCLOUD_ZONE
* GCLOUD_CLUSTER
* GCLOUD_SERVICEACCOUNT_BASE64

## Changes

See [CHANGELOG.md][changelog] for changes.


## Maintainer

[Markus Reil](https://bitbucket.org/mreil-com/)


## Contribute

Please raise issues [here](../../issues).

Feel free to address existing issues and raise a [pull request](../../pull-requests).


## License

[MIT][license]


[readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[readme-link]: https://github.com/RichardLitt/standard-readme
[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
[license]: LICENSE
[changelog]: CHANGELOG.md
[hub]: https://hub.docker.com/r/mreil/kube-deploy/
