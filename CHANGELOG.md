# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.8.0-1] - 2017-10-08
### Added
- First released version
- Add Google Cloud SDK
- Add kubectl 1.8.0


[0.1.1]: ../../branches/compare/rel-0.1.0..rel-0.1.1#diff
[1.8.0-1]: ../../src/rel-1.8.0-1
[Unreleased]: ../../src
