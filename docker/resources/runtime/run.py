#!/usr/bin/env python

import argparse
import base64
from subprocess import call, Popen, PIPE


def parse_args():
    parser = argparse.ArgumentParser(description='Apply a Kubernetes deployment.')
    parser.add_argument('--file',
                        dest='file',
                        help='deployment file (default: /deployment.yml)')
    parser.add_argument('--gcloud-projectid',
                        dest='gcloud_projectid',
                        help='Google Cloud Project ID (can also be provided by environment variable'
                             ' GCLOUD_PROJECTID)')
    parser.add_argument('--gcloud-zone',
                        dest='gcloud_zone',
                        help='Google Cloud Zone (e.g. us-central1-a, can also be provided by environment'
                             ' variable GCLOUD_ZONE)')
    parser.add_argument('--gcloud-cluster',
                        dest='gcloud_cluster',
                        help='Google Container cluster (can also be provided by environment'
                             ' variable GCLOUD_CLUSTER)')
    parser.add_argument('--gcloud-serviceaccount-file',
                        dest='gcloud_serviceaccount_file',
                        help='Location of the service account JSON file (can also be provided by environment'
                             ' variable GCLOUD_SERVICEACCOUNT_FILE). Either this or'
                             ' --gcloud-serviceaccount-base64 is required.')
    parser.add_argument('--gcloud-serviceaccount-base64',
                        dest='gcloud_serviceaccount_base64',
                        help='Base 64 string of the service account JSON file (can also be provided by environment'
                             ' variable GCLOUD_SERVICEACCOUNT_BASE64). Either this or'
                             ' --gcloud-serviceaccount-file is required.')

    return parser.parse_args()


args = parse_args()

if args.gcloud_projectid:
    call(['gcloud', 'config', 'set', 'project', args.gcloud_projectid])

if args.gcloud_zone:
    call(['gcloud', 'config', 'set', 'compute/zone', args.gcloud_zone])

if args.gcloud_serviceaccount_base64:
    proc = Popen(['gcloud', 'auth', 'activate-service-account', '--key-file=-'],
                 stdin=PIPE)
    proc.stdin.write(base64.decodestring(args.gcloud_serviceaccount_base64))
    proc.stdin.close()
    proc.wait()

    call(['gcloud', 'container', 'clusters', 'get-credentials', args.gcloud_cluster])

call(['kubectl', 'get', 'pods', '--all-namespaces'])

print "FILE: %s" % args.file
print "FILE: %s" % (args.file != "")

print "Deploying %s" % args.file
call(['kubectl', 'apply', '-f', args.file])
