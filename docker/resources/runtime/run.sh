#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${DIR}/run.py" \
    --gcloud-projectid "${GCLOUD_PROJECTID}" \
    --gcloud-zone "${GCLOUD_ZONE}" \
    --gcloud-serviceaccount-base64 "${GCLOUD_SERVICEACCOUNT_BASE64}" \
    --gcloud-cluster "${GCLOUD_CLUSTER}" \
    --file "${DEPLOYMENT}"
