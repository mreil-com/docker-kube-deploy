#!/usr/bin/env bash

set -ex

apt update -y
apt install -y wget software-properties-common apt-transport-https curl vim

VERSION=$(lsb_release -c -s)
CLOUD_SDK_REPO="cloud-sdk-${VERSION}"
KUBE_REPO="kubernetes-${VERSION}"

wget -O - https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

add-apt-repository "deb http://apt.kubernetes.io/ ${KUBE_REPO} main"
add-apt-repository "deb http://packages.cloud.google.com/apt ${CLOUD_SDK_REPO} main"

apt update -y
apt install -y kubectl google-cloud-sdk

apt remove -y wget software-properties-common apt-transport-https
/setup/common/clean.sh
